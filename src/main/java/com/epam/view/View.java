package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.controller.ControllerIml;
import com.epam.model.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);


    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    public View(Controller controller) {
        this.controller = controller;
    }
        public View() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Put after print values and keys of BinaryTree");
        menu.put("Q", "  Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("Q", this::pressButton6);
    }

    private void pressButton1() {
        new ControllerIml(new BinaryTree());

    }

    private void pressButton2() {

        logger1.info(controller);
    }

    private void pressButton3() {
        logger1.info(controller);
    }

    private void pressButton4() {
        logger1.info(controller);
    }

    private void pressButton5() {
        logger1.info(controller);
    }

    private void pressButton6() {
        logger1.info("Bye-Bye");
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nEXCEPTIONS:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


