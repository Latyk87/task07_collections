package com.epam.controller;

import com.epam.Application;
import com.epam.model.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    private static Logger logger1 = LogManager.getLogger(Application.class);
    BinaryTree binaryTree;

    public ControllerIml(BinaryTree binaryTree) {
        this.binaryTree = binaryTree;
        for (int i = 1; i < 20; i++) {
            int a = i + 2;
            binaryTree.put(i, a);

        }
        logger1.info(binaryTree.keySet() + " Keys");
        logger1.info(binaryTree.values() + " Values");

    }
}